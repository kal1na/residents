package ua.od.atomspace.resident.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResidentDto {

  private String firstName;

  private String lastName;

  private Integer salary;

  private Integer age;

}
