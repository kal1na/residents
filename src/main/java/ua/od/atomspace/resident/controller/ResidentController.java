package ua.od.atomspace.resident.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ua.od.atomspace.resident.dto.ResidentDto;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ResidentController {

  private static final List<ResidentDto> residents = new ArrayList<>();

  @GetMapping("/residents/initiate")
  public String initiate(Model model) {
    ResidentDto residentDto1 = ResidentDto.builder()
        .age(1)
        .firstName("Resident")
        .lastName("Surname")
        .salary(11000100)
        .build();

    ResidentDto residentDto2 = ResidentDto.builder()
        .age(10)
        .firstName("Resident2")
        .lastName("Surname33")
        .salary(1100)
        .build();

    ResidentDto residentDto3 = ResidentDto.builder()
        .age(1000)
        .firstName("Resident23333")
        .lastName("S44urname3444443")
        .salary(1144400)
        .build();
    residents.add(residentDto1);
    residents.add(residentDto2);
    residents.add(residentDto3);
    return "redirect:/residents";
  }

  @GetMapping("/residents")
  public String list(Model model) {
    model.addAttribute("residents", residents);
    return "residents";
  }

}
